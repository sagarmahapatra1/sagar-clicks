---
title : Useful Resources
sidebar: false

---

# Useful Resources




<Preview url="https://permaculturenews.org/2017/09/05/permaculture-design-5-steps/" ></Preview>




<Preview url="https://www.tenthacrefarm.com/6-maps-permaculture-farm-design/" ></Preview>



<Preview url="https://www.youtube.com/watch?v=lHLutKWVNME&" ></Preview>




<Preview url="https://costfordblog.wordpress.com/2014/07/10/build-with-bamboo-bring-home-nature/" ></Preview>



<Preview url="https://www.youtube.com/watch?v=9bT5gFyXJZE&" ></Preview>



<Preview url="https://www.youtube.com/watch?v=zYgU9XWrarY&" ></Preview>




<Preview url="https://www.youtube.com/watch?v=u3cvek-fHIY&&t=32s" ></Preview>



<Preview url="https://www.youtube.com/watch?v=2GeUiIrQH9M&" ></Preview>



<Preview url="https://www.youtube.com/watch?v=Q_m_0UPOzuI&" ></Preview>

